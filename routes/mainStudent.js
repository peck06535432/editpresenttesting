const express = require('express')
const router = express.Router()
const studentController = require('../controller/StudentsController')
/* GET users listing. */
router.get('/', studentController.getStudents)

router.get('/:id', studentController.getStudent)

module.exports = router
