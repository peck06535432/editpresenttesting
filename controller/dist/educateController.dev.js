"use strict";

var User = require('../models/Educate.js');

var userController = {
  userList: [],
  lastId: 3,
  addUser: function addUser(req, res, next) {
    var payload, user;
    return regeneratorRuntime.async(function addUser$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            payload = req.body;
            user = new User(payload);
            _context.prev = 2;
            _context.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            res.json(user);
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](2);
            res.status(500).send(_context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[2, 8]]);
  },
  updateUser: function updateUser(req, res, next) {
    var payload, user;
    return regeneratorRuntime.async(function updateUser$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            payload = req.body;
            _context2.prev = 1;
            _context2.next = 4;
            return regeneratorRuntime.awrap(User.updateOne({
              _id: payload._id
            }, payload));

          case 4:
            user = _context2.sent;
            res.json(user);
            _context2.next = 11;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            res.status(500).send(_context2.t0);

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  deleteUser: function deleteUser(req, res, next) {
    var id, users;
    return regeneratorRuntime.async(function deleteUser$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            id = req.params.id;
            _context3.prev = 1;
            _context3.next = 4;
            return regeneratorRuntime.awrap(User.deleteOne({
              _id: id
            }));

          case 4:
            users = _context3.sent;
            res.json(users);
            _context3.next = 11;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](1);
            res.status(500).send(_context3.t0);

          case 11:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  getUsers: function getUsers(req, res, next) {
    var users;
    return regeneratorRuntime.async(function getUsers$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return regeneratorRuntime.awrap(User.find({}));

          case 3:
            users = _context4.sent;
            res.json(users);
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](0);
            res.status(500).send(_context4.t0);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  getUser: function getUser(req, res, next) {
    var id, users;
    return regeneratorRuntime.async(function getUser$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            id = req.params.id;
            _context5.prev = 1;
            _context5.next = 4;
            return regeneratorRuntime.awrap(User.findById(id));

          case 4:
            users = _context5.sent;
            res.json(users);
            _context5.next = 11;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](1);
            res.status(500).send(_context5.t0);

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  // List controller
  addUserInList: function addUserInList(req, res, next) {
    var id, payload, user;
    return regeneratorRuntime.async(function addUserInList$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            id = req.params.id;
            payload = req.body;
            console.log(payload);
            _context6.prev = 3;
            _context6.next = 6;
            return regeneratorRuntime.awrap(User.updateOne({
              _id: id
            }, {
              $push: {
                lists: payload
              }
            }));

          case 6:
            user = _context6.sent;
            res.json(user);
            _context6.next = 13;
            break;

          case 10:
            _context6.prev = 10;
            _context6.t0 = _context6["catch"](3);
            res.status(500).send(_context6.t0);

          case 13:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[3, 10]]);
  },
  updateUserInList: function updateUserInList(req, res, next) {
    var id, payload, user;
    return regeneratorRuntime.async(function updateUserInList$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            id = req.params.id;
            payload = req.body;
            _context7.prev = 2;
            _context7.next = 5;
            return regeneratorRuntime.awrap(User.updateOne({
              _id: id,
              'lists._id': payload._id
            }, {
              $set: {
                'lists.$.keyId': payload.keyId,
                'lists.$.lname': payload.lname,
                'lists.$.status': payload.status,
                'lists.$.checked': payload.checked
              }
            }));

          case 5:
            user = _context7.sent;
            res.json(user);
            _context7.next = 12;
            break;

          case 9:
            _context7.prev = 9;
            _context7.t0 = _context7["catch"](2);
            res.status(500).send(_context7.t0);

          case 12:
          case "end":
            return _context7.stop();
        }
      }
    }, null, null, [[2, 9]]);
  },
  deleteUserInList: function deleteUserInList(req, res, next) {
    var id, payload, user;
    return regeneratorRuntime.async(function deleteUserInList$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            id = req.params.id;
            payload = req.body;
            console.log(req.body);
            _context8.prev = 3;
            _context8.next = 6;
            return regeneratorRuntime.awrap(User.updateOne({
              _id: id,
              'lists._id': payload._id
            }, {
              $pull: {
                lists: payload
              }
            }));

          case 6:
            user = _context8.sent;
            res.json(user);
            _context8.next = 13;
            break;

          case 10:
            _context8.prev = 10;
            _context8.t0 = _context8["catch"](3);
            res.status(500).send(_context8.t0);

          case 13:
          case "end":
            return _context8.stop();
        }
      }
    }, null, null, [[3, 10]]);
  }
};
module.exports = userController;