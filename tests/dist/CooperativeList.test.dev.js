"use strict";

var dbhandler = require('./db-handler');

var Cooperative = require('../models/Cooperative');

beforeAll(function _callee() {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(dbhandler.connect());

        case 2:
        case "end":
          return _context.stop();
      }
    }
  });
});
afterEach(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(dbhandler.clearDatabase());

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
});
afterAll(function _callee3() {
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(dbhandler.closeDatabase());

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  });
});
var cooperativeComplete = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{
    keyId: '60160270',
    lname: 'Supawee Test1',
    status: true
  }, {
    keyId: '57160270',
    lname: 'Supawee Test2',
    status: false
  }]
};
var cooperativeComplete2 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{
    keyId: '1234567',
    lname: 'HEY',
    status: true
  }]
};
var cooperativeIncomplete = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{
    keyId: '1234567',
    lname: 'Supawee Test1',
    status: true
  }, {
    keyId: '123456789',
    lname: 'Supawee Test2',
    status: false
  }]
};
var cooperativeIncomplete2 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{
    keyId: '1234567',
    lname: 'Su',
    status: true
  }, {
    keyId: '123456789',
    lname: '',
    status: false
  }]
};
var cooperativeIncomplete3 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{
    keyId: '60160270'
  }]
};
var cooperativeIncomplete4 = {
  name: 'Company Test 1',
  date: new Date(),
  address: 'Test 1 Address',
  detail: 'Test 1 Detail',
  lists: [{
    keyId: '123456789',
    lname: '',
    status: 'NOPE'
  }]
};
describe('Cooperative', function () {
  it('สามารถเพิ่มรายชื่อได้ ', function _callee4() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            error = null;
            _context4.prev = 1;
            cooperative = new Cooperative(cooperativeComplete);
            _context4.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](1);
            error = _context4.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('สามารถเพิ่มรายชื่อได้ ชื่อนิสิต เท่ากับ 3 ตัวอักษร', function _callee5() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            error = null;
            _context5.prev = 1;
            cooperative = new Cooperative(cooperativeComplete2);
            _context5.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context5.next = 10;
            break;

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](1);
            error = _context5.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มรายชื่อได้ ไม่ได้ใส่ข้อมูลชื่อนิสิต และสถานะนิสิต', function _callee6() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            error = null;
            _context6.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete3);
            _context6.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context6.next = 10;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](1);
            error = _context6.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มรายชื่อได้ รหัสนิสิตน้อยกว่าหรือมากกว่า 8 ตัวอักษร', function _callee7() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            error = null;
            _context7.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete);
            _context7.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context7.next = 10;
            break;

          case 7:
            _context7.prev = 7;
            _context7.t0 = _context7["catch"](1);
            error = _context7.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context7.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มรายชื่อได้ ชื่อนิสิต น้อยกว่า 3 ตัวอักษร', function _callee8() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            error = null;
            _context8.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete2);
            _context8.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context8.next = 10;
            break;

          case 7:
            _context8.prev = 7;
            _context8.t0 = _context8["catch"](1);
            error = _context8.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context8.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มรายชื่อได้ สถานะนิสิต ไม่อยู่ใน True False Null', function _callee9() {
    var error, cooperative;
    return regeneratorRuntime.async(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            error = null;
            _context9.prev = 1;
            cooperative = new Cooperative(cooperativeIncomplete4);
            _context9.next = 5;
            return regeneratorRuntime.awrap(cooperative.save());

          case 5:
            _context9.next = 10;
            break;

          case 7:
            _context9.prev = 7;
            _context9.t0 = _context9["catch"](1);
            error = _context9.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context9.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
});