const dbhandler = require('./db-handler')
const Educate = require('../models/Educate')

beforeAll(async () => {
  await dbhandler.connect()
})

afterEach(async () => {
  await dbhandler.clearDatabase()
})

afterAll(async () => {
  await dbhandler.closeDatabase()
})

const showListsEducateComplete = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', lname: 'Supawee', status: true, checked: true },
    { keyId: '60160253', lname: 'Pichaiyut', status: true, checked: true }
  ],
  type: '1'
}

const showListsEducateComplete2 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', lname: 'Supawee', status: false, checked: false },
    { keyId: '60160253', lname: 'Pichaiyut', status: false, checked: false }
  ],
  type: '1'
}

const showListsEducateComplete3 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', lname: 'Supawee', status: null, checked: null },
    { keyId: '60160253', lname: 'Pichaiyut', status: null, checked: null }
  ],
  type: '1'
}

const showListsEducateIncomplete = {
  name: '',
  date: new Date(),
  maxsize: '',
  address: '',
  detail: '',
  lists: [],
  type: ''
}

const showListsEducateErrorKeyIdEmpty = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '', lname: 'Supawee', status: true, checked: true },
    { keyId: '', lname: 'Pichaiyut', status: true, checked: true }
  ],
  type: '1'
}

const showListsEducateErrorKeyId7Alphabets = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '6016027', lname: 'Supawee', status: true, checked: true },
    { keyId: '6016025', lname: 'Pichaiyut', status: true, checked: true }
  ],
  type: '1'
}

const showListsEducateErrorKeyId9Alphabets = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '601602700', lname: 'Supawee', status: true, checked: true },
    { keyId: '601602533', lname: 'Pichaiyut', status: true, checked: true }
  ],
  type: '1'
}

const showListsEducateErrorLnameEmpty = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', lname: '', status: true, checked: true },
    { keyId: '60160253', lname: '', status: true, checked: true }
  ],
  type: '1'
}

const showListsEducateErrorLname2Alphabets = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', lname: 'Su', status: true, checked: true },
    { keyId: '60160253', lname: 'Pi', status: true, checked: true }
  ],
  type: '1'
}

const showListsEducateErrorStatusValid = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', lname: 'Supawee', status: 'A', checked: true },
    { keyId: '60160253', lname: 'Pichaiyut', status: 'A', checked: true }
  ],
  type: '1'
}

describe('Educate', () => {
  it('สามารถเพิ่มการอบรมได้ ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateComplete)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่มการอบรมได้เมื่อค่า status เป็น false ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateComplete2)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่มการอบรมได้เมื่อค่า status เป็น null ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateComplete3)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateIncomplete)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้เพราะ KeyId เป็น ช่องว่าง ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateErrorKeyIdEmpty)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้เพราะ KeyId เป็น น้อยกว่า 8 ตัว ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateErrorKeyId7Alphabets)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้เพราะ KeyId เป็น มากกว่า 8 ตัว ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateErrorKeyId9Alphabets)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้เพราะ lname เป็น ช่องว่าง ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateErrorLnameEmpty)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้เพราะ lname เป็น น้อยกว่า 3 ตัว ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateErrorLname2Alphabets)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้เพราะ status ไม่ได้เป็น true, false, null ', async () => {
    let error = null
    try {
      const educate = new Educate(showListsEducateErrorStatusValid)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
