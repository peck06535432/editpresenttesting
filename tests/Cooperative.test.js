const dbhandler = require('./db-handler')
const Cooperative = require('../models/Cooperative')

beforeAll(async () => {
  await dbhandler.connect()
})

afterEach(async () => {
  await dbhandler.clearDatabase()
})

afterAll(async () => {
  await dbhandler.closeDatabase()
})

const cooperativeComplete = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
}

const cooperativeIncomplete1 = {
  name: '',
  date: new Date(),
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
}

const cooperativeIncomplete2 = {
  name: 'TEST',
  date: null,
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
}

const cooperativeIncomplete3 = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: '',
  lists: []
}

const cooperativeIncomplete4 = {
  name: '',
  date: null,
  address: 'Bangkok',
  detail: 'Testing',
  lists: []
}

const cooperativeIncomplete5 = {
  name: 'TEST',
  date: null,
  address: 'Bangkok',
  detail: '',
  lists: []
}

const cooperativeIncomplete6 = {
  name: '',
  date: new Date(),
  address: 'Bangkok',
  detail: '',
  lists: []
}

const cooperativeIncomplete7 = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: 'Tester',
  lists: []
}

const cooperativeIncomplete8 = {
  name: 'TEST',
  date: new Date(),
  address: 'Bangkok',
  detail: 'Test',
  lists: []
}

const cooperativeIncomplete9 = {
  name: '',
  date: null,
  address: 'Bangkok',
  detail: '',
  lists: []
}

describe('Cooperative', () => {
  it('สามารถเพิ่มการประกาศได้ ', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeComplete)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete1)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีวันที่ปิดรับสมัคร', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete2)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีข้อมูลรายละเอียด', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete3)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท และ วันที่ปิดรับสมัคร', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete4)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีวันที่ปิดรับสมัคร และ ข้อมูลรายละเอียด', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete5)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท และ ข้อมูลรายละเอียด', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete6)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ข้อมูลรายละเอียดต้องมีมากกว่า 6 ตัวอักษร', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete7)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ข้อมูลรายละเอียดต้องมีมากกว่า 6 ตัวอักษร', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete8)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการประกาศได้ เพราะ ไม่มีชื่อบริษัท และ วันที่ปิดรับสมัคร และ ข้อมูลรายละเอียด', async () => {
    let error = null
    try {
      const cooperative = new Cooperative(cooperativeIncomplete9)
      await cooperative.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
