const mongoose = require('mongoose')
const Schema = mongoose.Schema

const docSchema = new Schema({
  ชื่อเอกสาร: String,
  ลิ้ง: String
})
module.exports = mongoose.model('Doc', docSchema)
