"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var educateSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 2
  },
  date: {
    type: Date,
    required: true
  },
  maxsize: {
    type: Number,
    required: true,
    minlength: 2
  },
  address: {
    type: String,
    required: true
  },
  detail: {
    type: String,
    required: true
  },
  lists: [{
    keyId: {
      type: String,
      minlength: 8,
      maxlength: 8,
      required: true
    },
    lname: {
      type: String,
      required: true
    },
    status: {
      type: Boolean,
      "enum": [true, false, null],
      required: true
    },
    checked: {
      type: Boolean,
      "enum": [true, false, null],
      required: true
    }
  }],
  type: {
    type: String,
    required: true
  }
});
module.exports = mongoose.model('educates', educateSchema);